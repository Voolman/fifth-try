import 'package:flutter/material.dart';

var theme = ThemeData(
  textTheme: const TextTheme(
    titleLarge: TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 24,
      fontFamily: "Roboto",
      color: Color.fromARGB(255, 58, 58, 58)
    ),
    titleMedium: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14,
        fontFamily: "Roboto",
        color: Color.fromARGB(255, 129, 129, 129)
    ),
    labelMedium: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16,
        fontFamily: "Roboto",
        color: Color.fromARGB(255, 255, 255, 255)
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: const Color.fromARGB(255, 106, 139, 249),
      disabledBackgroundColor: const Color.fromARGB(255, 167, 167, 167),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4))
    )
  )
);