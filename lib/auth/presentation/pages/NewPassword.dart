import 'package:flutter/material.dart';
import 'package:training7/auth/domain/NewPasPresenter.dart';
import 'package:training7/common/widgets/CustomTextField.dart';
import '../../domain/utils.dart';
import 'Holder.dart';
import 'LogIn.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});

  @override
  State<NewPassword> createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Новый пароль',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Введите новый пароль',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                  label: "Пароль",
                  hint: "**********",
                  controller: passwordController,
                  onChanged: (text){},
                  enableObscure: true,
                ),
                CustomTextField(
                  label: "Повторите пароль",
                  hint: "**********",
                  controller: passwordConfirmController,
                  onChanged: (text){},
                  enableObscure: true,
                ),
                const SizedBox(height: 411),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        pressNewPas(
                            passwordController.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                                (String e){showError(context, e);}
                        );
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Подтвердить',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  ),
                ),
                const SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                  child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                            ),
                            TextSpan(
                                text: 'Вернуться',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))
                            )
                          ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}