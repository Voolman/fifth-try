import 'package:flutter/material.dart';
import 'package:training7/auth/domain/SignInPresenter.dart';
import 'package:training7/auth/domain/utils.dart';
import 'package:training7/auth/presentation/pages/Holder.dart';
import '../../../common/widgets/CustomTextField.dart';
import 'ForgotPassword.dart';
import 'SignUp.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isChanged = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Добро пожаловать',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Заполните почту и пароль чтобы продолжить',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                    label: "Почта",
                    hint: "***********@mail.com",
                    controller: emailController,
                    onChanged: (text){}
                ),
                CustomTextField(
                  label: "Пароль",
                  hint: "**********",
                  controller: passwordController,
                  onChanged: (text){},
                  enableObscure: true,
                ),
                const SizedBox(height: 18),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          height: 22,
                          width: 22,
                          child: Checkbox(
                            value: isChanged,
                            onChanged: (value) {
                              setState(() {
                                isChanged = value!;
                              });
                            },
                            activeColor: const Color.fromARGB(255, 106, 139, 249),
                            checkColor: Colors.white,
                            side: const BorderSide(width: 1, color: Color.fromARGB(255, 129, 129, 129)),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                          ),
                        ),
                        const SizedBox(width: 8),
                        Text(
                          'Запомнить меня',
                          style: Theme.of(context).textTheme.titleMedium,
                        )
                      ],
                    ),
                    GestureDetector(
                      onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ForgotPassword()));},
                      child: Text(
                        "Забыли пароль?",
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 371),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: () {
                        pressSignIn(
                            emailController.text,
                            passwordController.text,
                            (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                            (String e){showError(context, e);}
                        );
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Войти',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const SignUp()));},
                  child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: "У меня нет аккаунта! ",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                            ),
                            TextSpan(
                                text: 'Создать',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))
                            )
                          ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}