import 'package:flutter/material.dart';
import 'package:training7/auth/domain/ForgotPasPresenter.dart';
import 'package:training7/auth/presentation/pages/OTPVerification.dart';
import 'package:training7/common/widgets/CustomTextField.dart';
import '../../domain/utils.dart';
import 'LogIn.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

TextEditingController emailController = TextEditingController();

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Восстановление пароля',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Введите свою почту',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                    label: "Почта",
                    hint: "***********@mail.com",
                    controller: emailController,
                    onChanged: (text){}
                ),
                const SizedBox(height: 503),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: () {
                        pressSendOTP(
                                emailController.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const OTPVerification()));},
                                (String e){showError(context, e);}
                        );
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Отправить код',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                  child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                            ),
                            TextSpan(
                                text: 'Вернуться',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))
                            )
                          ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}