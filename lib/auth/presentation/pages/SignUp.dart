import 'package:flutter/material.dart';
import 'package:training7/auth/domain/SignUpPresenter.dart';
import 'package:training7/common/widgets/CustomTextField.dart';

import '../../domain/utils.dart';
import 'Holder.dart';
import 'LogIn.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Создать аккаунт',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Завершите регистрацию чтобы начать',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                    label: "Почта",
                    hint: "***********@mail.com",
                    controller: emailController,
                    onChanged: (text){}
                ),
                CustomTextField(
                    label: "Пароль",
                    hint: "**********",
                    controller: passwordController,
                    onChanged: (text){},
                    enableObscure: true,
                ),
                CustomTextField(
                    label: "Повторите пароль",
                    hint: "**********",
                    controller: passwordConfirmController,
                    onChanged: (text){},
                    enableObscure: true,
                ),
                const SizedBox(height: 319),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: () {
                      pressSignUp(
                          emailController.text,
                          passwordController.text,
                              (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                              (String e){showError(context, e);}
                      );
                    },
                      style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Зарегистрироваться',
                      style: Theme.of(context).textTheme.labelMedium,
                    )
                  ),
                ),
                const SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LogIn()));},
                  child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "У меня уже есть аккаунт! ",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                          ),
                          TextSpan(
                            text: 'Войти',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))
                          )
                        ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}