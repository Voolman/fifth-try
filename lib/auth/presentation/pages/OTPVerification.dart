import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:training7/auth/domain/ForgotPasPresenter.dart';
import 'package:training7/auth/domain/OTPPresenter.dart';
import 'package:training7/auth/presentation/pages/ForgotPassword.dart';
import '../../domain/utils.dart';
import 'LogIn.dart';
import 'NewPassword.dart';

class OTPVerification extends StatefulWidget {
  const OTPVerification({super.key});

  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}

class _OTPVerificationState extends State<OTPVerification> {
  TextEditingController codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Верификация',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Введите 6-ти значный код из письма',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 58),
                Pinput(
                  length: 6,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  controller: codeController,
                  defaultPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(0),
                          border: Border.all(
                              color:
                                  const Color.fromARGB(255, 129, 129, 129)))),
                  submittedPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(0),
                          border: Border.all(
                              color:
                                  const Color.fromARGB(255, 106, 139, 249)))),
                ),
                const SizedBox(height: 48),
                GestureDetector(
                  onTap: () {
                    pressSendOTP(emailController.text, () {}, (String e) {
                      showError(context, e);
                    });
                  },
                  child: Text(
                    'Получить новый код',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                        color: const Color.fromARGB(255, 106, 139, 249)),
                  ),
                ),
                const SizedBox(height: 319),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: () {
                        pressOTP(emailController.text, codeController.text, () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => const NewPassword()));
                        }, (String e) {
                          showError(context, e);
                        });
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Сбросить пароль',
                        style: Theme.of(context).textTheme.labelMedium,
                      )),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text: "Я вспомнил свой пароль! ",
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(fontWeight: FontWeight.w700)),
                    TextSpan(
                        text: 'Вернуться',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(
                                color:
                                    const Color.fromARGB(255, 106, 139, 249)))
                  ])),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
