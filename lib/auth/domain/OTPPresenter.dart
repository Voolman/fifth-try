import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training7/auth/data/repository/supabase.dart';

void pressOTP(String email, String code, Function onResponse, Function onError){
  try{
    verificationOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}