import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training7/auth/data/repository/supabase.dart';

void pressNewPas(String password, Function onResponse, Function onError){
  try{
    changePas(password);
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}