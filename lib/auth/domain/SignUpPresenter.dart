import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training7/auth/data/repository/supabase.dart';


  void pressSignUp(String email, String password, Function onResponse, Function onError){
    try{
      signUp(email, password);
      onResponse();
    }on AuthException catch(e){
      onError(e);
    }
  }

