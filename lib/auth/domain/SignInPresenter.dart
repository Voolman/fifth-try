import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training7/auth/data/repository/supabase.dart';

  void pressSignIn(String email, String password, Function onResponse, Function onError){
    try{
      signIn(email, password);
      onResponse();
    }on AuthException catch(e){
      onError(e);
    }
  }
