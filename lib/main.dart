import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'auth/presentation/pages/SignUp.dart';
import 'common/theme.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://hawusttfcqcomedevycj.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imhhd3VzdHRmY3Fjb21lZGV2eWNqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDk3MzQwNDIsImV4cCI6MjAyNTMxMDA0Mn0.FaTVbgSsA1uzIp5miFfUdTH4-G7cnIL9CwGs0rOJ1Hs',
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: theme,
      home: const SignUp()
    );
  }
}
